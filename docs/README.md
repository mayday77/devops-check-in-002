[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
# DevOps Assignment

This is a check-in for DevOps engineers related to the assignment/tasks.



- - -



# Orientation & Instruction

This repository contains a simple program written in Golang named `pinger`. This service reponds with `"hello world"` at the root path and can be configured to ping another server through the environment variables (see the file at `./cmd/pinger/config.go`). By default, running it will make it start a server and ping itself.

A basic `Makefile` is provided that allows you to:

- pull in dependencies - `make dep`
- builds the binaries - `make build`
- test runs - `make run`
- run tests - `make test`


## Pre-requisites

You will need the following installed:

- `go` to run the application (check with `go version`)
- `docker` for image building/publishing (check with `docker version`)
- `docker-compose` for environment provisioning (check with `docker-compose version`)
- `git` for source control (check with `git -v`)
- `make` for simple convenience scripts (check with `make -v`)

You will also need to clone the below Gitlab repository
- URL - https://gitlab.com/mayday77/devops-check-in-002
- SSH - git@gitlab.com:mayday77/devops-check-in-002.git
- https - https://gitlab.com/mayday77/devops-check-in-002.git



## Directory structure

| Directory | Description |
| --- | --- |
| `/bin` | Contains binaries |
| `/cmd` | Contains source code for CLI interfaces |
| `/deployments` | Contains image files and manifests for deployments |
| `/docs` | Contains documentation |
| `/vendor` | Contains dependencies (use `make dep` to populate it) |
| `/build` | Contains docker image |



## Task Overview

**There are 4 main tasks** covered in the below with details and instructions.

## Get Started

1. Clone this repository
2. Create your own repository on GitLab
3. Set your local repository's remote to point to your GitLab repository
4. Make your changes locally according to the tasks below if applicable
5. Push to your GitLab repository



- - -



# Containerisation


## Task

Create a `Dockerfile` in the `./deployments/build` directory according to any best practices you may know about. You may write tests as you see fit.

## Steps
1. First compile the binary needed for the dockerfile on your work machine, however please note the below.
   > Note: Depending on the operation system in which the docker image will be based on, e.g Windows, Linux, MacOS(darwin) etc, the golang compilation method might differ which results in different formats of executable.                                                                                                                              
   > For e.g, if you compile the binary on your MacOS(darwin) and attempt to place this in a linux based docker container, you will see the following error when trying to instantiate it from the image.
   > 
   > ![](.README_images/format_error.png)   
   >
   > To overcome this, you can edit the following lines in the Makefile. In this example, the intended docker container is linux based.                                                                                                                      
   > (Add GOOS and GOARCH parameters)
   > ```
   > build: # builds the binary
   >        GO111MODULE=on GOOS=linux GOARCH=amd64 go build -mod vendor -o ./bin/pinger ./cmd/pinger
   > ```
2. You can then use the following commands from the root repository folder.
   ```
   # installs dependencies
   make dep;
   # to build the binary
   make build;
   ```
   (you can force build the binary with make -B build if you encounter "build is up to date" message)
3. The binary file pinger will be created under ./bin folder in which the docker file will retrieve from.

4. Run the docker build command.
   ```
   make docker_image;
   ```

## Result
1. Verify that the image is created.
   ```
   docker image ls
   ```
   ![](.README_images/image_ls.png)
   
## Test

1. Running `docker run -it -p 8000:8000 devops/pinger:latest` should result in the same behaviour as running `go run ./cmd/pinger`.
2. Or run the following which is doing the same thing as point 1. The container starts the service listening on port 8000 internally and pings itself as well. 
    ```
    make docker_testrun;
    ```
    ![](.README_images/docker_testrun.png)
    
# Pipeline

## Task

Create a pipeline that results in:

1. The binary being built
2. Docker image being built

The following should also be exposed as GitLab job artifacts:

1. The binary itself
2. Docker image in `.tar` format

## Steps
1. A gitlab pipeline file `.gitlab-ci.yml` is created in the root of repository directory.
2. To view the current pipeline code, do the following
   ```
   make verify_pipeline;
   ```
   ![](.README_images/pipeline_file.png)
   
3. To run the pipeline, either commit changes to the master branch or trigger it manually from the Gitlab CI/CD pipeline GUI **(master branch)**.

   ![](.README_images/pipeline_GUI.png)

4. There are 2 stages to the pipeline.
   - Stage 1: Build Binaries from within the agent runner.
     * The agent container runner will compile and build the binaries as well as publishing the binary as a job artifact.
       Console output as below.
       ![](.README_images/build_bin.png)
       
   - Build the docker image from within the agent runner.
     * The agent container will take the published artifact from stage 1 and create a docker image. It will spin up a container for a quick test and the image will then be saved and published as a job artifact.
       ![](.README_images/build_docker.png)       

## Result
Job artifacts will be generated when the pipeline is finished successfully.
1. Binary artifact
   ![](.README_images/bin_art.png)

2. Docker image artifact
   ![](.README_images/bin_docker.png)



- - -

# Environment


## Task

A `docker-compose.yml` is created in the `./deployments` to demonstrate two `pinger` services that ping each other.
Running `docker-compose -f ./deployments/docker-compose.yml up -V` should result in a network of at least 2 Docker containers that are pinging each other with the other acting as an echo server. Exposing the logs should reveal them pinging each other at their different ports.


## Steps
1. The docker-compose file is created in `./deployments` and below is the code.
   ![](.README_images/docker_compose.png)

2. The docker compose file will make use of the existing image `devops/pinger:latest` and spin up 2 containers.
    - pinger1 container
      * This container will be listening on port 8100 internally and will ping the other container on port 8200 using the service name.
    - pinger2 container
      * This container will be listening on port 8200 internally and will ping the other container on port 8100 using the service name.

3. You can use the following command to run the docker-compose file.      
    ```sh
    make testenv;
    ```
   > Note: A change was made in the Makefile to run this command.
   >
   > From: docker-compose up -f ./deployments/docker-compose.yml -V
   >
   > To: docker-compose -f ./deployments/docker-compose.yml up -V

   ![](.README_images/docker_pingers.png)
   
   (Ctrl-C to stop the containers if you wish)

4. From the output, pinger1_1 container started listening on port 8100 internally.
   ![](.README_images/pinger1_port.png)
   
5. From the output, pinger2_1 container started listening on port 8200 internally.
   ![](.README_images/pinger2_port.png)
   
## Test
1. While the docker compose is still running, you can check the logs of the 2 docker containers.
2. To view the logs of the first container pinger1, use the following command.
   ```
   docker logs <container ID|container Name>
   ```
   * pinger1 listening on port 8100
   * server line shows a GET request from pinger2 (IP) 
   * service line shows a GET request to pinger2 on port 8200
   
     ![](.README_images/pinger1_logs.png)
     
   * pinger2 listening on port 8200
   * server line shows a GET request from pinger1 (IP) 
   * service line shows a GET request to pinger1 on port 8100
     
     ![](.README_images/pinger2_logs.png)     
- - -


# Documentation

## Task

Explanation/Details are written in this current README.md file in the `./docs` directory.


## Test

You can run the following to view the source README.md file.

```sh
make verify_readme;
```



- - -



# Versioning (Bonus)

Note that this requires that the [Pipeline Section](#pipeline) is complete.

> **REMINDER**: this provides an additional 20% to your assessment


## Context

When referring to problems, we often use a version number. The (arguably) leading way to do this is via semver (eg. 1.15.2). Let's apply versioning to what we did!


## Task

Your pipeline probably has multiple stages (regardless of in YAML structure/in logic), add additional scripting to bump the version of this repository using Git tags, and add this version to the produced Docker image too. The versioning strategy is up to you to decide.


## Deliverable

On the GitLab CI pipeline page, we can manually trigger a CI pipeline run. Assuming you are at version X, triggering a CI pipeline run should bump the version to version Y, where X comes before Y in any logical sequence.



- - -

# License

Code is licensed under the [MIT license](./LICENSE).

Content is licensed under the [Creative Commons 4.0 (Attribution) license](https://creativecommons.org/licenses/by-nc-sa/4.0/).



- - -

